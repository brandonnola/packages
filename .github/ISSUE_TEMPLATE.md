**Notes:**

- **We will delete issues not following this template!**
- **You can add new information besides the ones required here.**
- **You should delete the subsections that are not related to your issue.**

##  👶 New package requests

- Link to the package(s) in AUR: [insert the link here](https://example.com)

- **Utility** this package has for you

  *This is the place to describe how you use the package and how it can be helpful.*

- Do you consider the package(s) to be useful for **every** Chaotic-AUR user?

  - [ ] YES!
  - [ ] No, but for a great amount.
  - [ ] No, but for a few.
  - [ ] No, it's useful only for me.

- Do you consider the package to be **useful for feature testing**/preview (e.g: mesa-aco, wine-wayland)?

  - [ ] Yes
  - [ ] No

- Are you sure the package **isn't available already** (test with `pacman -Ss <pkgname>`) ?

  - [ ] Yes

- Have you tested if the package builds in a [**clean chroot**](https://wiki.archlinux.org/title/DeveloperWiki:Building_in_a_clean_chroot)?

  - [ ] Yes
  - [ ] No

- Does the package's **license** allow redistributing it?

  - [ ] YES!
  - [ ] No clue.
  - [ ] No, but the author doesn't really care anyways. It's just for bureaucracy.

- Have you searched the [issues](https://github.com/chaotic-aur/packages/issues) to ensure this request **is unique**?

  - [ ] YES!

- Have you read the [README](https://github.com/chaotic-aur/packages#banished-and-rejected-packages) to ensure this package is **not banned**?

  - [ ] YES!

## 👴 Reporting outdated packages

- If available: link to latest build ([find it here](https://builds.garudalinux.org/repos/chaotic-aur/logs/)): [some-package.log](https://builds.garudalinux.org/repos/chaotic-aur/logs/some-package.log)

- Package name: `some-package`

- Latest build: `v0.0.0.r0.gc0ffec0ffe`

- Latest version available: `v1.0.0.r100.gdeadbeef`

- Have you tested if the latest version of the package builds in a clean chroot?

  - [ ] Yes
  - [ ] No

## 🐛 Reporting bugs

- What happens?

  *This is the place to describe whats happening*

- What is expected to happen?

  This is the place to describe whats expected*

- If possible, please attach logs.

## 🔓 Security reports

- Depending on how critical is making this information available, please send it to [critical at chaotic dot cx](mailto:critical@chaotic.cx)

- Include a brief description of the attacking vector, the possible outcomes and, if possible, solutions.

